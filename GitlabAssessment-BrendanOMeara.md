# Support Engineer Take-Home Assessment
## Question 1  

### First Script

        #!/bin/bash  
        Awk -F: ‘{print $1, “:” $6}’ /etc/passwd  

### Second Script

        #!/bin/bash

        md5sum firstScript.sh > /var/log/current_users

        if md5sum -c /var/log/current_users; then
                :
        else

                date > /var/log/user_changes && md5sum firstScript.sh > /var/log/current_users
        fi

  
### Crontab Entry

        0 * * * * /home/user/firstScript.sh 
        0 * * * * /home/user/secondScript.sh


## Question 2

**Support Ticket#** - “Long load time for a page on web application”

Environment: 
* The web application is written in a modern MVC web framework.
* Application data is stored in a relational database.
* All components (web application, web server, database) are running on a single Linux box with 8GB RAM, 2 CPU cores, and SSD storage with ample free space.
* You have root access to this Linux box.

Questions to confirm with user:
* Is only one page experiencing this issue?
* Other slowness experienced on the network?
* What other pages are affected(if any)?
* Continual issue or once off?
* What is the content of the page in question?

Notes: 
* Environment seems appropriate for web application
* Check server logs for error messages (var/log/)


In this situation I would confirm the above questions with the user and check the server logs on the Linux box for any errors. Any errors that might be found will lead the investigation further. If only one page is taking a long time to load, then it’s possible that it is not optimised correctly. Perhaps there are large media files on this page causing the load time to take longer than other pages. 



## Question 3

[Commit Graph](image2.png)  
Assuming the git repository has been initialised already, the following commands would create the above commit graph.

* Git add .
* Git commit -m “first commit”
* Git add .
* Git commit -m “second commit”

* Git branch feature-branch
* Git checkout feature-branch
* Git add .
* Git commit -m “awesome feature”

* Git checkout main
* Git add .
* Git commit -m “third commit”

* Git merge feature-branch

* Git add .
* Git commit -m “fourth commit”


## Question 4
### Using Git to implement a new feature/change without affecting the main branch

As the title suggests, this tutorial will walk you through one of the key capabilities of the git tool. Starting from the main branch, we will use the git tool to create a new branch, move to said branch, and make a new feature/change on this new branch. 

The purpose of this capability is to test changes in parallel to the main branch without having an effect. Once changes are fully tested on a new branch, users can decide to merge those changes to the main branch instead of carrying out the same workload twice. 

We will start this tutorial assuming you have already configured Git, created a folder, and initialised your repository. If you haven’t gotten this far, please follow our previous ‘Getting Started with Git’ tutorial [here]( ).

STEP 1  
From _Illustration:1_ below, we can see that we have one branch currently. Using the **'git branch’** command lists the current branch in use along with any other existing branches available. In Git, a branch is a separate version of the main repository. NOTE: The asterisk (*) next to the branch shows which branch out of the list of branches the user is currently on.

The **‘git status’** command also shows this information along with any changes to be committed to the repository.  
[Illustration:1](image1.png)   

STEP 2  
We will now create a new branch called ‘feature1’ which we will use to make changes to a file without affecting the ‘master’ branch. Use the command **‘git branch feature1’** to create this new branch. 
Once created, use the **‘git branch’** command again to see if the new branch was created. Note in _Illustration:2_ that creating a branch doesn’t change over to the new branch.  
[Illustration:2](image3.png)   

STEP 3  
To switch our workspace to this new branch, we must use the **‘git checkout feature1’** command. This ‘checkout’ command is what’s used to check out a branch, essentially moving us to whatever branch you desire to work on. 

As show in Illustration:3 We used the checkout command to move to our new branch. We then confirmed this using the **‘git branch’** command. As you can see, the new branch is highlighted and the asterisk(*) has shifted from ‘master’ to ‘feature’.  
[Illustration:3](image4.png)   

STEP 4  
Now that we have shifted our workspace to a new branch, we can make any necessary tests/changes without affecting the main branch. For example, we can make changes to an existing file and create a new file all while in the same directory as the ‘master’ branch. 

For this example, we have an existing ‘testfile.txt.’ that we will add a new line to. Once edited, we then created a new ‘featurebranch-testfile.txt’ and added some text.

After making changes, we can use the **‘git status’** command to check the status of the current branch. In _Illustration:4_ The ‘git status’ command shows changes to an existing file, along with a new untracked file.  
[Illustration:4](image5.png)   


STEP 5  
Next, we are going to add all changes made to commit. This is the same process as committing a file in the main branch, but this time we are on the ‘feature1’ branch. To add these changes you can use the **‘git add’** command with the files ready for commit. In this case, we will use the **‘git add --all’**  command as we want to add all the changes made for commit. Upon doing this, use the ‘git status’ command again to check that our command worked. See _Illustration:5_.  
[Illustration:5](image6.png)   

STEP 6  
Lastly, we will commit these changes to our ‘feature1’ branch just like we would commit changes to our main branch. In _Illustration:6_ we used **‘git commit -m “edited testfile and added new featurefile” ‘**. The message gives a brief idea of what changes were made. 

Once completed, we can change back to our ‘master’ branch to see that our ‘featurebranch-testfile.txt is not present. The new line within the existing ‘testfile.txt’ will also not be present. This demonstrates the capability of using git to run separate changes to the same repository in parallel.  
[Illustration:6](image7.png)    

This concludes our tutorial on using git to implement a new feature without affecting the main branch. To now merge these changes to the main branch, please review our other tutorial that walks you through this process [here]( ).


## Question 5

I recently read an introduction to Web3 article on the Ethereum website. Found [here](https://ethereum.org/en/web3/).

What I enjoyed about this article is it put the previous and current web situation into context before diving into this new Web3 technology. Breaking each Web stage into individual parts, it allows the reader to understand how the Web grew from what it was when it began, to what it is becoming today. 

The article lists the limitations of Web3 adoption and mentions at one point that educational initiatives are vital for its success. I would agree that educational initiatives are helpful, but I believe the majority of people learn new technologies by using them. 

I believe what is more vital to Web3 success is its continual development and adoption by startups and existing companies. With this adoption, comes new and interesting web applications that end users will enjoy. With this adoption, comes learning. Users of the existing web require necessity and interest to move forward by peaking their interest in this new technology and its capabilities. With this, its adoption will continue. 

I especially enjoyed the idea of ownership of digital assets. The ability to transfer digital assets such as NFTs or in-game currencies to new platforms is an incredible capability. This ensures the time and money spent on one platform can be transferable to another, meaning your efforts are not wasted when you move to a new platform. I believe this will open the door to a new wave of possibilities for end users and grow the online world exponentially. 
